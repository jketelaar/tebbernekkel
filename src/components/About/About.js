import "./about.scss";

export default function About() {
    return (
        <section className="topsection-awards-container">
            <div className="awards-container">
                <div className="award-container-desktop-2021">
                    <h1>Awards 2021</h1>
                    <h3>Silver <b>ADCN</b> Lamp
                        <br></br>
                        <i>Entertainment</i></h3>
                    <h3>Bronze <b>ADCN</b> Lamp
                        <br></br>
                        <i>Activation</i></h3>
                    <h3><b>Gouden Kalf</b>
                        <br></br>
                        <i>Best Drama</i></h3>
                    <h3>Silver <b>Effie</b>
                        <br></br>
                        <i>Behaviour Short</i></h3>
                    <h3>Bronze <b>Spin</b> Award
                        <br></br>
                        <i>Digital Campaign</i></h3>
                    <h3><b>Webby</b> Award
                        <br></br>
                        <i>Corporate Social Responsibility</i></h3>
                    <h3><b>Webby</b> Nominee</h3>
                    <h3>Bronze <b>Esprix</b>
                        <br></br>
                        <i>Business to Business & Employee Experience</i></h3>
                    <h3><b>SAN Accent</b> Nomination</h3>
                    <h3>2<small>x</small> <b>Dutch Creativity</b> Awards Finalist
                        <br></br>
                        <i>Direct & Strategic Brand Ideas</i></h3>
                    <h3><b>Youtube Works</b> Award
                        <br></br>
                        <i>Creative Innovation</i></h3>
                    <h3>3<small>x</small> <b>Youtube Works</b> Awards Finalist</h3>
                    <h3><b>Best Social</b> Award
                        <br></br>
                        <i>Best Campaign</i></h3>
                </div>
                <div className="award-container-desktop-2020">
                    <h1>Awards 2020</h1>
                    <h3><b>Eurobest</b> Grand Prix
                        <br></br>
                        <i>Creative Strategy</i></h3>
                    <h3><b>Eurobest</b> Bronze
                        <br></br>
                        <i>Brand Experience & Activation</i></h3>
                    <h3>2<small>x</small> <b>Eurobest</b> Shortlist
                        <br></br>
                        <i>Direct & Digital</i></h3>
                    <h3>3<small>x</small> Silver <b>Lovie</b> Award
                        <br></br>
                        <i>Digital Campaign</i> </h3>
                    <h3>Bronze <b>Lovie</b> Award
                        <br></br>
                        <i>Best video campaign</i></h3>
                    <h3>4<small>x</small> Peoples <b>Lovie</b> Award</h3>
                    <h3><b>Lovie</b> Award Shortlist</h3>
                    <h3><b>Grand Prix</b> Content Marketing</h3>
                </div>
            </div>
        </section>
    )
}